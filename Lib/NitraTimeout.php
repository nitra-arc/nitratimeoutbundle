<?php
/**
 * Вызов PHP кода с временным ограничением
 * Пример использования
 *  
 * $timeout = $this->container->get('nitra.timeout');
 * $val = $timeout->process(function(){
 *  $foo = 0;
 *  while($foo < 100000000) {
 *      ++$foo;
 *  }
 *  return $foo;
 * }, 2000000);
 */

namespace Nitra\TimeoutBundle\Lib;

use Symfony\Component\DependencyInjection\Container;
use Nitra\TimeoutBundle\Lib\NitraTimeoutException;


/**
 * NitraTimeout
 */
class NitraTimeout
{
    
    /**
	 * получить текущее время в микросекундах 
	 * @return float
	 */
	private function getMicrotime()
	{
		list($m, $s) = explode(' ', microtime());
		return round(((float)$m + (float)$s) * 1000000);
	}
    
    /**
     * проверить лимит времени
     * @param float $timeEnd - microseconds, not miliseconds
     * @throw NitraTimeoutException время вышло
     */
    private /*static*/ function checkTick($timeEnd)
    {
        // время сейчас
        $timeNow = $this->getMicrotime();
        
        // проверить текущее время и время отведенное на выполнение
        if ($timeNow >= $timeEnd) {
            throw new NitraTimeoutException('Time out.');
        }
    }
    
    /**
     * выполнить $func 
     * в лимите времени $microseconds
     * @param mixed Closure or string $func
     * @param integer $microseconds - микросекунды (1 сек = 1000000)
     * @throw NitraTimeoutException если $func не выполнилась за время $microseconds 
     * @return mixed $func - результат $func
     */
    public function process($func, $microseconds)
    {
        
        // зарегистрировать метод проверки лимита
        register_tick_function(array(&$this, 'checkTick'), $this->getMicrotime() + $microseconds);
        
        // попытка выполнить в лимите времени 
        try {
            
            // выполнить $func
            declare(ticks = 1) {
                
                // если $func не функция замыкания
                if ($func instanceof \Closure) {
                    // выполнить функцию замыкания
                    $result = $func();
                    
                } elseif(is_string($func)) {
                    //создать и выполнить виртуальную функцию
                    $func = create_function('', $func);
                    $result = call_user_func($func);
                    
                } else {
                    // убрать метод проверки лимита
                    unregister_tick_function(array($this, 'checkTick'));
                    // ошибка
                    throw new \InvalidArgumentException("Не верно указан параметр \"func\"");
                }
                
            }
            
            // метод выполнился в лимите времени успешно
            // убрать метод проверки лимита
            unregister_tick_function(array($this, 'checkTick'));
            
            // вернуть результат выполнения $func
            return $result;
            
        } catch(NitraTimeoutException $e) {
            // метод выполнился в лимите времени успешно
            // убрать метод проверки лимита
            unregister_tick_function(array($this, 'checkTick'));
        }
        
        // вернуть null 
        // $func не был выполнен в допустимом лимите времени
        return null;
    }
    
}
