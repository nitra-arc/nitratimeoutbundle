<?php

function getMicrotime()
{
    list($m, $s) = explode(' ', microtime());
    return round(((float) $m + (float) $s) * 1000000);
}

function checkTick($timeEnd)
{
    // проверить текущее время и время отведенное на выполнение
    if (getMicrotime() >= $timeEnd) {
        throw new \Exception('Time out.');
    }
}

function process($func, $microseconds)
{
    
    // зарегистрировать метод проверки лимита
    register_tick_function('checkTick', getMicrotime() + $microseconds);

    // выполнить в лимите времени 
    try {
        
        // выполнить $func
        declare(ticks = 1) {
            
            if ($func instanceof \Closure) {
                $result = $func();
                
            } elseif (is_string($func)) {
                $func = create_function('', $func);
                $result = call_user_func($func);
                
            } else {
                die("\Error!");
            }
        }

        // метод выполнился в лимите времени успешно
        // убрать метод проверки лимита
        unregister_tick_function('checkTick');

        // вернуть результат выполнения $func
        return $result;
    } catch (\Exception $e) {
        
        // метод выполнился в лимите времени успешно
        // убрать метод проверки лимита
        unregister_tick_function('checkTick');
    }

    // вернуть null 
    // $func не был выполнен в допустимом лимите времени
    return null;
}

// выполнить в лимите времени
$microseconds = 1000000;
$result = null;
$result = process(function() { $foo = 0; while ($foo<100000000) { ++$foo; } return $foo; }, $microseconds);
//$result = process('$foo = 0; while ($foo<100000000) { ++$foo; } return $foo;', $microseconds);
//$result = process('$a=10; $b=10; return ($a+$b);', $microseconds);
echo "\n\n$ result = "; var_dump($result);
echo "\n\n";

