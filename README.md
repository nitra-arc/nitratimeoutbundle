# NitraTimeoutBundle #

Ограничение по времени выполнения PHP кода

*****

## [Подключение](#markdown-header-_2) ##

* [composer.json](#markdown-header-composerjson)
* [app/AppKernel.php](#markdown-header-appappkernelphp)

*****

## [Пример](#markdown-header-_5) ##

* [controller.php](#markdown-header-controllerphp)

*****

### Подключение ###

#### composer.json ####
```json
{
    ...
    "require": {
        ...
        "nitra/timeout-bundle": "dev-master",
        ...
    }
    ...
}
```

#### app/AppKernel.php ####
```php
<?php
//...
class AppKernel extends Kernel
{
    //...
    public function registerBundles()
    {
        //...
        $bundles = array(
            //...
            // NitraTimeoutBundle
            new Nitra\TimeoutBundle\NitraTimeoutBundle(),
            //...
        );
        //...
        return $bundles;
    }
    //...
}
```

*****

### Пример отправки ###

#### controller.php ####
```php
// ...
        // получить сервис
        $timeout = $this->container->get('nitra.timeout');
        
        // выполнить php код с ограничением по времени
        $val = $timeout->process(function(){
            $foo = 0;
            while($foo < 100000000) {
                ++$foo;
            }
            return $foo;
        }, 1000000);
// ...
```